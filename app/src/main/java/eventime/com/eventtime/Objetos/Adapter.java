package eventime.com.eventtime.Objetos;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import java.util.List;

import eventime.com.eventtime.PostEventos;
import eventime.com.eventtime.R;

public class Adapter extends RecyclerView.Adapter<Adapter.EventosViewHolder>{
    private StorageReference mStorage;
    private List<Eventos> eventos;
    private Context context;

    public Adapter(List<Eventos> eventos) {
        this.eventos = eventos;
    }

    //Holder de REcyclerview
    @Override
    public EventosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View  v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_recycler, parent, false);
        EventosViewHolder holder = new EventosViewHolder(v);
        context = parent.getContext();
        return holder;
    }

    @Override
    public void onBindViewHolder(EventosViewHolder holder, int position) {
        final Eventos post = eventos.get(position);//cursor que recorre los valores de la tabla eventos
        Glide.with(context).load("https://firebasestorage.googleapis.com/v0/b/event-time-4833c.appspot.com/o/20180602_171613.png?alt=media&token=9a2ea613-32c1-42ff-a31b-35939fe90469")
                .fitCenter().into(holder.ivFotoEvento);
        holder.tvTitulo.setText("Titulo: " + post.getTitulo()); //muesstra dato que hay en la llave descripcion de base de datos firebase
        holder.tvLugar.setText("Lugar: " + post.getLugar()); //muestra lugar
        holder.tvFechaHora.setText("Fecha y hora: " + post.getFechaHora()); //muestra la fecha y hora

        holder.parent_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent publicacion = new Intent(context, PostEventos.class);
                publicacion.putExtra("tituloEvento", post.getTitulo());
                publicacion.putExtra("LugarEvento", post.getLugar());
                publicacion.putExtra("FechaHoraEvento", post.getFechaHora());
                publicacion.putExtra("DescripcionEvento", post.getDescripcion());
                publicacion.putExtra("AutorEvento", post.getAutor());
                context.startActivity(publicacion);
            }
        });
    }

    @Override
    public int getItemCount() {
        return eventos.size();
    }

    public static class EventosViewHolder extends RecyclerView.ViewHolder{
        ImageView ivFotoEvento;
        TextView tvTitulo, tvLugar, tvFechaHora;
        LinearLayout parent_layout;
        public EventosViewHolder(View itemView) {
            super(itemView);
            tvTitulo = (TextView) itemView.findViewById(R.id.tvTituloEvento);
            ivFotoEvento = (ImageView) itemView.findViewById(R.id.ivEvento);
            tvLugar = (TextView) itemView.findViewById(R.id.tvLugarEvento);
            tvFechaHora = (TextView) itemView.findViewById(R.id.tvFechaHoraEvento);
            parent_layout = (LinearLayout) itemView.findViewById(R.id.parent_layout);
        }
    }
}
