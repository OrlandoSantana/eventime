package eventime.com.eventtime.Objetos;

public class Eventos {
    String titulo;
    String autor;
    String descripcion;
    String lugar;
    String fechaHora;
    String imageUrl;

    public Eventos(String titulo, String autor, String descripcion, String lugar, String fechaHora, String imageUrl) {
        this.titulo = titulo;
        this.autor = autor;
        this.descripcion = descripcion;
        this.lugar = lugar;
        this.fechaHora = fechaHora;
    }
    public String getAutor() {        return autor;    }

    public void setAutor(String autor) {        this.autor = autor;    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getTitulo() { return titulo;  }

    public void setTitulo(String titulo) { this.titulo = titulo; }

    public Eventos() {
    }
}
