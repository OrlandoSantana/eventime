package eventime.com.eventtime;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class PostEventos extends AppCompatActivity {

    TextView tvTitulo, tvFechaHora,tvDescripcion, tvLugar, tvAutor;
    ImageView ivEventTime;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_eventos);
        tvTitulo = (TextView) findViewById(R.id.tvTituloPostEvento);
        tvLugar = (TextView) findViewById(R.id.tvLugarPostEvento);
        tvDescripcion = (TextView) findViewById(R.id.tvDescripcionPostEvento);
        tvFechaHora = (TextView) findViewById(R.id.tvFechaHoraPostEvento);
        tvAutor = (TextView) findViewById(R.id.tvAutorPostEvento);
        ivEventTime = (ImageView) findViewById(R.id.ivPostEvento);
        mostrarDatos();
        Glide.with(this)
                .load("https://firebasestorage.googleapis.com/v0/b/event-time-4833c.appspot.com/o/20180602_171613.png?alt=media&token=9a2ea613-32c1-42ff-a31b-35939fe90469")
                .fitCenter()
                .into(ivEventTime);
    }

    private void mostrarDatos(){
        if (getIntent().hasExtra("tituloEvento")
                && getIntent().hasExtra("LugarEvento")
                && getIntent().hasExtra("FechaHoraEvento")
                &&getIntent().hasExtra("DescripcionEvento")
                &&getIntent().hasExtra("AutorEvento")){

            String titulo = getIntent().getStringExtra("tituloEvento");
            String lugar = getIntent().getStringExtra("LugarEvento");
            String FechaHora = getIntent().getStringExtra("FechaHoraEvento");
            String Descripcion = getIntent().getStringExtra("DescripcionEvento");
            String Autor = getIntent().getStringExtra("AutorEvento");
            tvTitulo.setText(titulo);
            tvLugar.setText("Lugar: " + lugar);
            tvFechaHora.setText("Fecha y hora del evento: " + FechaHora);
            tvDescripcion.setText("Descripción: " + Descripcion);
            tvAutor.setText("Publicado por: " + Autor);
        }

    }

}
