package eventime.com.eventtime;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import eventime.com.eventtime.Objetos.Eventos;
import eventime.com.eventtime.Objetos.Referencia;

public class publicar extends AppCompatActivity {
    private StorageReference mStorage;//referencia storage firebase
    ImageView imageViewFoto;
    Button publicar;
    EditText txtDescripcion, txtLugar, txtFechaHora, txtTitulo;
    static final int COD_SUBIRFOTO = 10; //codigo para subir la foto
    private Uri uriFoto;  //objeto Uri que sirve para conseguir la imagen del celular
    FirebaseAuth mAuth;  //Autentificacion de Firebase

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publicar);
        publicar = (Button) findViewById(R.id.btnPublicarEvento);
        imageViewFoto = (ImageView) findViewById(R.id.imageViewFotoPublicar);
        txtTitulo = (EditText) findViewById(R.id.txtTituloEvento);
        txtDescripcion = (EditText) findViewById(R.id.txtDescripcion);
        txtLugar = (EditText) findViewById(R.id.txtLugarEvento);
        txtFechaHora = (EditText) findViewById(R.id.txtFechaHora);
        mAuth = FirebaseAuth.getInstance();
        mStorage = FirebaseStorage.getInstance().getReference();

        publicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String imageUrl = "";
                String titulo = txtTitulo.getText().toString();
                String descripcion = txtDescripcion.getText().toString();
                String lugar = txtLugar.getText().toString();
                String fechaHora = txtFechaHora.getText().toString();
                String autor = mAuth.getCurrentUser().getEmail();
                if (!titulo.isEmpty() && !descripcion.isEmpty()
                        && !lugar.isEmpty()
                        && !fechaHora.isEmpty()){
                    FirebaseDatabase evenTime = FirebaseDatabase.getInstance();  //Referencia a la base de datos firebase
                    DatabaseReference reference = evenTime.getReference(Referencia.REFERENCIA_PRINCIPAL);  //Referencia del nombre de la base de datos
                    Eventos eventos = new Eventos(titulo, autor, descripcion, lugar, fechaHora, imageUrl);     //datos que se sube a base datos firebase
                    reference.child(Referencia.REFERENCIA_EVENTOS).push().setValue(eventos); //añade esos valores a la tabla firebase
                    Toast.makeText(getApplicationContext(), "Su publicación fue exitosa", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), principal.class);
                    startActivity(intent);
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(), "Por favor llene todos los datos de su evento", Toast.LENGTH_SHORT).show();
                }
            }
        });

/*
        subirFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent subirFoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI); //Consigue abrir la galeria
                subirFoto.setType("image/"); //para elegir las diferentes aplicaciones de galeria que tenemos
                startActivityForResult(subirFoto.createChooser(subirFoto, "Selecciona la aplicación"), COD_SUBIRFOTO); //para seleccioar la aplicacion
            }
        });
    }

        protected void onActivityResult ( int requestCode, int resultCode, Intent data){
            super.onActivityResult(requestCode, resultCode, data);
            if (resultCode == RESULT_OK) {
                switch (requestCode) {
                    case COD_SUBIRFOTO:
                        uriFoto = data.getData();  //consigue la imagen seleccionada
                        imageViewFoto.setImageURI(uriFoto); //muestra la imagen en imageview
                        break;
                }
            }
        }


        private void subirFotoFirebase () {
            final StorageReference filepath = mStorage.child(txtDescripcion.getText().toString() + ".jpeg");
            filepath.putFile(uriFoto);
        */
    }
}

