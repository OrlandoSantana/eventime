package eventime.com.eventtime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class InterfazPrincipal extends AppCompatActivity {

    Button btnSesionInvitado, btnUsuarioAdministrador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interfaz_principal);
        btnSesionInvitado = (Button) findViewById(R.id.btnSesionInvitado);
        btnUsuarioAdministrador = (Button)findViewById(R.id.btnUsuarioAdministrador);

        btnUsuarioAdministrador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Login = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(Login);
                finish();
            }
        });

        btnSesionInvitado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Invitado = new Intent(getApplicationContext(), principalinvitado.class);
                startActivity(Invitado);
                finish();
            }
        });
    }
}
